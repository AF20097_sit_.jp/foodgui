import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KadaiGUI {
    public static int a=0;//単体の金額を格納
    void order(String food,int kk){
        //注文の確認
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        //注文完了の処理
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering! It will be served" +
                    " as soon as possible."
            );
            String currentText = Orderlist.getText();
            Orderlist.setText(currentText + food + " " + kk + "yen" + "\n");
            a += kk;
            total.setText("Total  " + a + "yen");
        }
    }
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton takanaButton;
    private JButton saradaButton;
    private JButton imoButton;
    private JTextPane Orderlist;
    private JButton checkButton;
    private JTextPane total;

    public static void main(String[] args) {
        JFrame frame = new JFrame("KadaiGUI");
        frame.setContentPane(new KadaiGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public KadaiGUI() {
        //tempuraのボタン設定
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Templa",120);
            }
        });
        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("tempura.jpg")));;
        //ramenのボタン設定
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",500);
            }
        });
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("ramen.jpg")));;
        //udonのボタン設定
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",200);
            }
        });
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("udon.jpg")));;
        //takanaのボタン設定
        takanaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("takana",100);
            }
        });
        takanaButton.setIcon(new ImageIcon( this.getClass().getResource("takana.jpg")));;
        //saradaのボタン設定
        saradaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sarada",100);
            }
        });
        saradaButton.setIcon(new ImageIcon( this.getClass().getResource("sarada.jpg")));;
        //imoのボタン設定
        imoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("imo",100);
            }
        });
        imoButton.setIcon(new ImageIcon( this.getClass().getResource("imo.png")));;
        //checkボタン設定
        checkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //お会計して良いか確認
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout? ",
                        "Order confimaiton",
                        JOptionPane.YES_NO_OPTION);
                //お会計の処理
                if(confirmation==0) {
                    Orderlist.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you .The total price is"
                            + a + " yen.");
                    a=0;
                    total.setText("Total  "+a+"yen");
                }
            }
        });
    }
}
